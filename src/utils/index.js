export default  {
  "success": true,
  "message": "查询成功",
  "code": 200,
  "result": {
    "allAuth": [
      {
        "action": "maintain:add",
        "describe": "添加",
        "type": "0",
        "status": "1"
      },
      {
        "action": "maintain:edit",
        "describe": "编辑",
        "type": "0",
        "status": "1"
      },
      {
        "action": "maintain:delete",
        "describe": "删除",
        "type": "0",
        "status": "1"
      },
      {
        "action": "maintain:export",
        "describe": "导出",
        "type": "0",
        "status": "1"
      }
    ],
    "auth": [
      {
        "action": "maintain:edit",
        "describe": "编辑",
        "type": "0"
      },
      {
        "action": "maintain:delete",
        "describe": "删除",
        "type": "0"
      },
      {
        "action": "maintain:add",
        "describe": "添加",
        "type": "0"
      },
      {
        "action": "maintain:export",
        "describe": "导出",
        "type": "0"
      }
    ],
    "menu": [
      {
        "redirect": "",
        "path": "/home",
        "component": "home",
        "route": "1",
        "meta": {
          "keepAlive": false,
          "internalOrExternal": false,
          "icon": "mdi-home-outline",
          "title": "首页"
        },
        "name": "home",
        "id": "home-101-id"
      },
      {
        "redirect": "/member/list",
        "path": "/member",
        "component": "article",
        "route": "1",
        "children": [
          {
            "path": "/member/list",
            "component": "member/list",
            "props": false,
            "route": "1",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "用户列表"
            },
            "name": "member-list",
            "id": "member-list-id"
          },
          {
            "path": "/fans/list",
            "component": "fans/list",
            "props": false,
            "route": "1",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "用户粉丝列表"
            },
            "name": "fans-list",
            "id": "fans-list-id"
          },
          {
            "path": "/memberserver/list",
            "component": "memberserver/list",
            // "props": true,
            "route": "2",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "服务商列表"
            },
            "name": "member-serverlist",
            "id": "member-serverlist-id"
          },
        ],

        "meta": {
          "keepAlive": false,
          "internalOrExternal": false,
          "icon": "mdi-owl",
          "title": "用户管理"
        },
        "name": "member",
        "id": "member-101-id"
      },
      {
        "redirect": "/stocklog/list",
        "path": "/stocklog",
        "component": "stocklog",
        "route": "1",
        "children": [
          {
            "path": "/stocklog/list",
            "component": "stocklog/list",
            "props": false,
            "route": "1",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "股权流水"
            },
            "name": "stocklog-list",
            "id": "stocklog-list-id"
          },
          {
            "path": "/accountloglist/list",
            "component": "accountloglist/list",
            "props": false,
            "route": "1",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "股权流水明细"
            },
            "name": "accountloglist-list",
            "id": "accountloglist-list-id"
          }
        ],
        "meta": {
          "keepAlive": false,
          "internalOrExternal": false,
          "icon": "mdi-owl",
          "title": "股权管理"
        },
        "name": "stocklog",
        "id": "stocklog-main-id"
      },

      {
        "redirect": "/ticket/list",
        "path": "/ticket",
        "component": "article",
        "route": "1",
        "children": [
          {
            "path": "/ticket/list",
            "component": "ticket/list",
            "props": false,
            "route": "1",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "优惠券列表"
            },
            "name": "ticket-list",
            "id": "ticket-list-id"
          }
        ],
        "meta": {
          "keepAlive": false,
          "internalOrExternal": false,
          "icon": "mdi-owl",
          "title": "优惠券"
        },
        "name": "stock",
        "id": "stockID"
      },

      {
        "redirect": "/role/list",
        "path": "/mnt",
        "component": "mnt",
        "route": "1",
        "children": [
          {
            "path": "/role/list",
            "component": "role/list",
            "route": "1",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "角色管理"
            },
            "name": "role-list",
            "id": "134397773"
          },
          {
            "path": "/admin/list",
            "component": "admin/list",
            "route": "1",
            "meta": {
              "keepAlive": false,
              "internalOrExternal": false,
              "title": "管理员管理"
            },
            "name": "admin-list",
            "id": "139021839876333977733"
          },

        ],
        "meta": {
          "keepAlive": false,
          "internalOrExternal": false,
          "icon": "mdi-owl",
          "title": "系统管理"
        },
        "name": "mnt",
        "id": "139021785589037875412"
      },


    ]
  },
  "timestamp": 1687250058905
};



