let t,u;
try {
  t = JSON.parse(localStorage.userInfo);
  u = JSON.parse(localStorage.userName);
} catch (err) {
  t = {};
}

export default {
  namespaced: true,
  state: {
    info: t,
    search: localStorage.userName || {},
    authorization: localStorage.Authorization || '',
  },
  getters: {},
  mutations: {
    updateInfo(s, v) {
      localStorage.userInfo = JSON.stringify(v);
      s.info = v;
    },
    updateAuthorization(s, v) {
      localStorage.Authorization = v;
      s.authorization = v;
    },
    updateSearch(s, v) {
      console.log('update search')
      // console.log(JSON.stringify(v))
      localStorage.userName = JSON.stringify(v);
      s.info = v;
    },
    clearSearch(s) {
      console.log('clear search')
      // localStorage.userName = '{}';
      // s.search = {};
    },
    clear(s) {
      delete localStorage.userInfo;
      s.info = {};
      delete localStorage.Authorization;
      s.authorization = '';
    },
  },
  actions: {},
};
