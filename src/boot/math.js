import { date } from 'quasar';

export const pointLength = (a) => {
  a += '';
  return a.indexOf('.') === -1 ? 0 : a.length - a.indexOf('.') - 1;
};

export const biger = (a, b) => (a > b ? a : b);
export const smaller = (a, b) => (a > b ? b : a);

export const numPlus = (a, b, c = true) => {
  a -= 0;
  b -= 0;
  if (a !== b) {
    const point = biger(pointLength(a), pointLength(b));
    a = Math.round(a * (10 ** point));
    b = Math.round(b * (10 ** point));
    if (Math.abs(a - b) > 1999) {
      a -= a < b ? -999 : 999;
    } else if (Math.abs(a - b) > 199) {
      a -= a < b ? -99 : 99;
    } else if (Math.abs(a - b) > 19) {
      a -= a < b ? -9 : 9;
    } else {
      a -= a < b ? -1 : 1;
    }
    if (point > 0) {
      a /= 10 ** point;
    }
  }
  if (c) {
    const t = a.toString().split('.');
    if (t.length > 1) {
      a = `${t[0]}.${t[1].padEnd(2, '0')}`;
    } else {
      a = `${t[0]}.00`;
    }
  }
  return a;
};

export const calendar = (y, m) => {
  const t = new Date();
  t.setFullYear(y, m -= 1, 1);
  t.setHours(0, 0, 0, 0);
  const w = t.getDay() - 1;
  const w1 = w < 1 ? w + 7 : w;
  const s = t.getTime() - w1 * 86400000;
  return Array(42).fill(0).map((v, i) => {
    const t1 = new Date(s + i * 86400000);
    return {
      d: t1,
      t: t1.getTime(),
      on: t1.getMonth() === m,
      day: t1.getDate(),
      i,
    };
  });
};

export const random = (max, min = 0) => Math.floor(Math.random() * (max - min)) + min;

export const array = (l, map) => [...Array(l)].map(map);

export const percent = (i, max, min = 0) => (i - min) / (max - min);

export const shuffle = (arr) => {
  const l = arr.length;
  arr = [...arr];
  for (let i = 1; i < l; i += 1) {
    const r = random(l);
    [arr[i], arr[r]] = [arr[r], arr[i]];
  }
  return arr;
};

export const treeFind = (s, l, sa, la) => {
  let t;
  if (l) {
    t = l.find((v) => v[sa] === s);
    let i = 0;
    while (!t && i < l.length) {
      t = treeFind(s, l[i][la], sa, la);
      i += 1;
    }
  }
  return t;
};

export const getPosition = (el) => {
  let left = el.offsetLeft;
  let top = el.offsetTop;
  let current = el.offsetParent;
  while (current !== null) {
    left += current.offsetLeft;
    top += current.offsetTop;
    current = current.offsetParent;
  }
  return { left, top };
};

export const ratio = (t) => {
  let c = 0;
  if (t < 1) {
    c = -1;
  } else if (t > 1) {
    c = 1;
  }
  return c;
};

export const dateX = {
  year() {
    const y = new Date().getFullYear();
    return array(10, (v, i) => (y - 9 + i));
  },
  month() {
    return array(12, (v, i) => (`${i + 1}月`));
  },
  day() {
    const end = date.endOfDate(new Date(), 'month');
    const m = end.getMonth() + 1;
    const d = end.getDate();
    return array(d, (v, i) => (`${m.toString().padStart(2, '0')}-${(i + 1).toString().padStart(2, '0')}`));
  },
  hour() {
    return array(10, (v, i) => (`${9 + i}:00`));
  },
};

/**
 * 时间戳转  2014-04-23 18:55:49  格式
 * @param {String} time  时间戳 (1398250549490)
 * @returns
 */
export const parseTime = (time) => {
  const nowdate = new Date(time);
  const Y = `${nowdate.getFullYear()}-`;
  const M = `${nowdate.getMonth() + 1 < 10 ? `0${nowdate.getMonth() + 1}` : nowdate.getMonth() + 1}-`;
  const D = `${nowdate.getDate()} `;
  const h = `${nowdate.getHours()}:`;
  const m = `${nowdate.getMinutes()}:`;
  const s = nowdate.getSeconds();
  return Y + M + D + h + m + s;
};

/**
 * 获取当前时间
 * @returns
 */
export const getNowTime = () => {
  const d = new Date();
  // 年 getFullYear()：四位数字返回年份
  const year = d.getFullYear(); // getFullYear()代替getYear()
  // 月 getMonth()：0 ~ 11
  const month = d.getMonth() + 1;
  // 日 getDate()：(1 ~ 31)
  const day = d.getDate();
  // 时 getHours()：(0 ~ 23)
  const hour = d.getHours();
  // 分 getMinutes()： (0 ~ 59)
  const minute = d.getMinutes();
  // 秒 getSeconds()：(0 ~ 59)
  const second = d.getSeconds();

  // 小于10的拼接上0字符串
  function addZero(s) {
    return s < 10 ? (`0${s}`) : s;
  }
  const time = `${year}-${addZero(month)}-${addZero(day)} ${addZero(hour)}:${addZero(minute)}:${addZero(second)}`;
  return time;
};

export const formatChart = (list, key) => {
  const t = [];
  list.forEach((v) => {
    const s = t.find((a) => a.name === v[key]);
    if (s) {
      s.data.push(v);
    } else {
      t.push({
        name: v[key],
        data: [v],
      });
    }
  });
  return t;
};

export const fillArray = (a, b, key1, key2, val) => a.map((v) => {
  const t = b.find((y) => y[key1] === v);
  return t ? t[key2] : val;
});
