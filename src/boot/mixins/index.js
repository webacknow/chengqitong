import ellipsisvalue from 'components/ellipsisvalue';

export const IndexMixin = {
  components: {
    ellipsisvalue,
  },
  data() {
    return {
      maximized: false,
      dialogSize: 'dialog_card column',
      showQuery: true,
      tableLabel: '展开',
      loading: false,
      importing: false,
      exporting: false,
      confirmMsg: '',
      selected: [],
      list: [],
      form: {},
      emptyForm: {},
      searchForm: {},
      key: '',
      username: '',
      mobile: '',
      catalog: '',
      pagination: {
        page: 1,
        rowsPerPage: 10,
        rowsNumber: 99,
      },
      editType: '',
      uploadUrl: '/quasarUpload',
      group: [],
      axios_posttype:'post'
    };
  },
  methods: {
    queryParam() {   //查询前处理参数，使用这个
      return {}
    },
    beforeQuery() {
      return true    //查询前处理了,终止了下面this.$axios.post事件
    },
    query(props) {
      if (!this.beforeQuery()) {
        return false
      }
      if (props && props.pagination) {
        this.pagination = props.pagination
      }
      this.loading = true
      // localStorage.setItem('searchform', JSON.stringify( this.searchForm));
      // const aa = localStorage.getItem('searchform');
      // localStorage.setItem('userName', JSON.stringify(this.searchForm) )
      // this.$store.commit("User/clearSearch");
      // this.$store.commit("User/updateSearch", JSON.stringify(this.searchForm));
      // console.log(this.searchForm)
      //const a = JSON.parse(localStorage.getItem('userName'))
      return this.$axios[this.axios_posttype](this.url.list, {
          ...this.queryParam(),
          ...this.searchForm,
          // key: this.key,
          page: this.pagination.page,
          limit: this.pagination.rowsPerPage,
      }).then((r) => {
        this.list = r.data.map((v, i) => {
          if (r.count > 1) {
            v.index = i + 1;
            //v.index = (r.current - 1) * r.size + (i + 1);
          } else {
            v.index = i + 1;
          }

          return v;
        });
        this.pagination.rowsNumber = r.count;
        // this.total_person = '总人数：' + this.pagination.rowsNumber
      }).finally(() => {
        this.loading = false;
      });
    },
    reset() {
      this.form = { ...this.emptyForm };
    },
    del({ id }) {
      this.loading = true;
      this.isEdit = 0
      return this.$axios.post(this.url.delete, { ids: id }).then((r) => {
        if(r.code === 0) {
          this.$q.notify({ position: 'center', timeout: 300, color: 'info', textColor: 'white', icon: 'cloud_done', message: r.msg});
        } else {
          this.$q.notify({ position: 'center', timeout: 300, color: 'info', textColor: 'white', icon: 'cloud_done', message: '删除失败',});
        }

        this.refresh()
        this.delAfter();
      }).finally(() => {
        this.query();
      });
    },
    delAfter() {

    },
    showConfirm() {
      this.confirmMsg = `确认删除这 ${this.selected.length} 条记录吗？`;
      this.$refs.confirmDialog.show();
    },
    deleteBatch() {
      this.loading = true;
      const ids = [];
      this.selected.forEach(({ id }) => { ids.push(id); });
      return this.$axios.delete(this.url.deleteBatch, { params: { ids: ids.join(',') } }).then((r) => {
        this.$info(r.msg);
        this.selected = [];
      }).finally(() => {
        this.query();
      });
    },
    addBefore() {
      return true;
    },
    ellipsis(value, vlength = 25) {
      if (!value) {
        return '';
      }
      if (value.length > vlength) {
        return `${value.slice(0, vlength)}...`;
      }
      return value;
    },
    add() {
      console.log('add')
      this.editType = '新建';
      this.isEdit = 0
      this.reset();
      if (this.addBefore()) {
        this.$refs.dialog.show();
      }
      this.addAfter();
    },
    addAfter() {

    },
    editBefore() {
      return true;
    },
    edit(p) {
      this.isEdit = 1
      if (this.editBefore(p)) {
        this.editType = '编辑';
        this.form = {
          ...p,
        };
        if(this.url.detail) {
          this.$axios.post(this.url.detail, { id: p.id })
            .then(
              (r) => {
                console.log(r.data.visibility)
                if(r.data.visibility == 1){
                  r.data.visibility = true
                }else{
                  r.data.visibility = false
                }
                if(r.data.top == 1){
                  r.data.top = true
                }else{
                  r.data.top = false
                }
                this.form = r.data
                if( this.url.detail == 'shopdetail') {
                  this.shop_model = this.form.shop_id
                  this.getShopList()
                }
                if( this.url.detail == 'shopmemberdetail') {
                  this.getShopList()
                }
                this.$refs.dialog.show();
              }
            );
        }
        this.$refs.dialog.show();
      }
      this.editAfter();

    },
    edit2(p , type) {
      console.log('edit2')
      this.isEdit = 1
      if (this.editBefore(p)) {
        this.editType = '编辑';
        this.form = {
          ...p,
        };
        this.$axios.post(this.url.detail, {id: p.id}).then(
          (r) => {
            console.log(r.data)
            this.form = r.data
            this.$refs.dialog.show();
          }
        );
        if(type == 'goods'){
          this.getModelList()

        }
        if(type == 'detail'){
          this.getModelList()
        }

      }
      this.editAfter();

    },
    refresh_(url){
      this.$axios.post(url, {page:1, limit:10}).then((r) => {
        this.list = r.data.map((v, i) => {
          if (r.count > 1) {
            v.index = i + 1;
            //v.index = (r.current - 1) * r.size + (i + 1);
          } else {
            v.index = i + 1;
          }
          return v;
        });
        this.pagination.rowsNumber = r.count;
      }).finally(() => {
        this.loading = false;
      });;
    },
    getModelList(){
      if(this.url.category && this.url.category  != undefined){
        this.$axios.post(this.url.category, { "limit":100,"page":1, "model":"category"} ).then(
          (r) => {
            for(let i = 0 ; i < r.data.length ; i++){
              this.modelList.push({
                label: r.data[i].name ,
                value: r.data[i].id,
              })
            }
          }
        );
      }
    },
    editAfter() {

    },

    importExcel() {
      this.$refs.excelUploader.pickFiles();
    },
    importedExcel({ xhr }) {
      this.$refs.excelUploader.removeUploadedFiles();
      const { response } = xhr;
      const res = JSON.parse(response);
      if (res.code === 200) {
        this.$info(res.message);
      } else {
        this.$error(res.message);
      }
      this.query();
    },
    exportExcel(fileName) {
      if (!fileName || typeof fileName !== 'string') {
        fileName = '导出文件';
      }
      this.exporting = true;
      this.$downFile(this.url.exportXlsUrl, {
        params:
        {
          ...this.queryParam(),
          ...this.searchForm,
          key: this.key,
          catalog: this.catalog,
          pageNo: 1,
          pageSize: 1000,
        },
      }).then((data) => {
        if (!data) {
          this.$message.warning('文件下载失败');
          return;
        }
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          window.navigator.msSaveBlob(new Blob([data], { type: 'application/vnd.ms-excel' }), `${fileName}.xls`);
        } else {
          const url = window.URL.createObjectURL(new Blob([data], { type: 'application/vnd.ms-excel' }));
          const link = document.createElement('a');
          link.style.display = 'none';
          link.href = url;
          link.setAttribute('download', `${fileName}.xls`);
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link); // 下载完成移除元素
          window.URL.revokeObjectURL(url); // 释放掉blob对象
        }
      }).finally(() => {
        this.exporting = false;
      });
    },

    delFile({ url }) {
      this.fileList = this.fileList.filter((v) => v.url !== url);
    },
    submit() {
      let q;
      if (this.editType === '新建') {
        q = this.$axios.post(this.url.add, this.form);
      } else {
        console.log(this.url.edit)
        q = this.$axios.post(this.url.edit, this.form);
      }
      return q.then((r) => {
        this.$info(r.message);

        this.$refs.dialog.hide();
      }).finally(() => {
        this.query();
      });
    },
    searchReset() {
      this.searchForm = {};
      this.query();
    },
    show() {
      this.showQuery = true;
      this.tableLabel = '收起';
    },
    hide() {
      this.showQuery = false;
      this.tableLabel = '展开';
    },
    copy(p) {
      this.$q.dialog({
        title: '复制',
        message: '请输新的名称',
        prompt: {
          model: '',
          outlined: true,
          isValid: (val) => val.length > 2,
          type: 'text',
        },
        cancel: true,
        persistent: true,
      }).onOk((data) => {
        this.$axios.post(`${this.url.copy}?id=${p.id}&name=${data}`, {}).then((r) => {
          this.$info(r.message);
          this.query();
        });
      });
    },
  },
  mounted() {
    // this.searchForm = {"status":"3" };
    if (this.$q.platform.is.mobile) {
      this.dialogSize = 'dialog_tool column';
      this.maximized = true;
    } else {
      this.dialogSize = 'dialog_card column';
      this.maximized = false;
    }
    this.query();
  },
  created() {
    this.columns.forEach(({ name }) => {
      this.group.push(name);
      this.emptyForm[name] = null;
    });
  },
  computed: {
    importExcelUrlFull() {
      return process.env.BASE_URL + this.url.importExcelUrl;
    },
  },

};
