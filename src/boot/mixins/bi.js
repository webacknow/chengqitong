import { themeMap, chartList, chartConfig } from 'boot/datatype';
import VueDraggableResizable from 'vue-draggable-resizable';
import 'vue-draggable-resizable/dist/VueDraggableResizable.css';
import backgroundsetting from 'pages/bi/screen/design/modules/setting/backgroundsetting';
import chartview from 'pages/bi/screen/design/modules/view/chartview';
import datasource from 'pages/bi/screen/design/modules/setting/datasource';
import chartsetting from 'pages/bi/screen/design/modules/setting/chartsetting';
import textview from 'pages/bi/screen/design/modules/view/textview';
import textsetting from 'pages/bi/screen/design/modules/setting/textsetting';
import imageview from 'pages/bi/screen/design/modules/view/imageview';
import imagesetting from 'pages/bi/screen/design/modules/setting/imagesetting';
import borderview from 'pages/bi/screen/design/modules/view/borderview';
import bordersetting from 'pages/bi/screen/design/modules/setting/bordersetting';
import decorationview from 'pages/bi/screen/design/modules/view/decorationview';
import decorationsetting from 'pages/bi/screen/design/modules/setting/decorationsetting';
import rankview from 'pages/bi/screen/design/modules/view/rankview';
import ranksetting from 'pages/bi/screen/design/modules/setting/ranksetting';
import videoview from 'pages/bi/screen/design/modules/view/videoview';
import videosetting from 'pages/bi/screen/design/modules/setting/videosetting';
import groupview from 'pages/bi/screen/design/modules/view/groupview';
import groupsetting from 'pages/bi/screen/design/modules/setting/groupsetting';
import omnipotentview from 'pages/bi/screen/design/modules/view/omnipotentview';
import omnipotentsetting from 'pages/bi/screen/design/modules/setting/omnipotentsetting';
import customview from 'pages/bi/screen/design/modules/view/customview';
import customsetting from 'pages/bi/screen/design/modules/setting/customsetting';
import charthouseview from 'pages/bi/screen/design/modules/view/charthouseview';
import charthousesetting from 'pages/bi/screen/design/modules/setting/charthousesetting';
import layout from 'pages/bi/screen/design/modules/layout';
import favorite from 'pages/bi/screen/design/modules/favorite';
import { dom } from 'quasar';

const { height, width } = dom;

export const BiMixin = {
  components: {
    VueDraggableResizable,
    layout,
    favorite,
    backgroundsetting,
    datasource,
    chartview,
    chartsetting,
    textview,
    textsetting,
    imageview,
    imagesetting,
    borderview,
    bordersetting,
    decorationview,
    decorationsetting,
    rankview,
    ranksetting,
    videoview,
    videosetting,
    groupview,
    groupsetting,
    omnipotentview,
    omnipotentsetting,
    customview,
    customsetting,
    charthouseview,
    charthousesetting,
  },
  data() {
    return {
      name: 'cc-admin',
      // 容器距离dom原点的偏移量
      shiftX: 0,
      shiftY: 0,
      // 容器的宽度和高度
      containerWidth: 0,
      containerHeight: 0,
      // 画布实际的0,0点坐标
      zeroLeft: 0,
      zeroTop: 0,
      // 通过移动这段距离，将画布和外边框中心点对齐
      moveLeft: 0,
      moveTop: 0,

      fullWidth: 1000,
      fullHeight: 650,
      onShfit: false,
      onCtrl: false,
      shiftMove: 1,
      move: 10,
      scale: 100,
      imgUrl: `${process.env.BASE_URL}/sys/common/static`,
      backgroundImage: '',
      themeMap,
      allParam: {}, // 所有请求参数
      left: true,
      right: true,
      moving: false, // 移动呢
      loading: false,
      saveLoading: false,
      selType: 'cursor',
      selChart: null,
      selChartArray: [],
      copyedItem: null,
      config: {
      },
      historyIndex: 0,
      history: [],
      backgroundConfig: {
        width: 800,
        height: 600,
        color: '#fff',
        showGrid: true,
        darkModel: false,
        src: '',
        backPicSet: 'repeat',
        opacity: 0,
        // 缩边距离
        zoom: 0.9,
      },
      index: 0,
      chartList,
      layout: [],
      eventLog: [],
      dragging: false,
      showDrawing: false,
      startX: 0,
      startY: 0,
      divX: 0,
      divY: 0,
      divWidth: 0,
      divHeight: 0,
      selectTool: '',
      url: {
        editConfig: '/bi/screen/editConfig',
      },
    };
  },
  watch: {
  },
  beforeDestroy() {
    document.documentElement.removeEventListener('mousemove', this.dottedLineMove, true);
  },
  computed: {
    clientScale() {
      return (this.containerWidth / this.backgroundConfig.width) * this.backgroundConfig.zoom;
    },
  },
  methods: {
    height,
    width,
    changeClientSize() {
      const container = document.getElementById('container');
      this.containerWidth = this.width(container);
      this.containerHeight = this.height(container);
      const shiftX = this.$refs.main.getBoundingClientRect().x;
      const shiftY = this.$refs.main.getBoundingClientRect().y;
      // 实现了画布和容器中心点对齐
      this.moveTop = (this.containerHeight - this.backgroundConfig.height) / 2;
      this.moveLeft = (this.containerWidth - this.backgroundConfig.width) / 2;
      const canvas = document.getElementById('canvas');
      canvas.style.top = `${this.moveTop}px`;
      canvas.style.left = `${this.moveLeft}px`;
      this.zeroLeft = (this.containerWidth * (1 - this.backgroundConfig.zoom)) / 2 + shiftX;
      this.zeroTop = (this.containerHeight - this.backgroundConfig.height * this.clientScale) / 2
                      + shiftY;
    },
    calcRealLeft(pos) {
      return (pos - this.zeroLeft) / this.clientScale;
    },
    calcRealTop(pos) {
      return (pos - this.zeroTop) / this.clientScale;
    },
    startDraw(e) {
      if (this.dragging) {
        e.preventDefault();
        this.startX = e.clientX;
        this.startY = e.clientY;
        this.divX = this.calcRealLeft(e.clientX);
        this.divY = this.calcRealTop(e.clientY);
        this.$refs.draw.style.left = `${this.divX}px`;
        this.$refs.draw.style.top = `${this.divY}px`;
        this.showDrawing = true;
      }
    },
    drawing(e) {
      if (this.showDrawing) {
        e.preventDefault();
        this.divWidth = e.clientX - this.startX > 0 ? e.clientX - this.startX : 0;
        this.divHeight = e.clientY - this.startY > 0 ? e.clientY - this.startY : 0;
        this.$refs.draw.style.width = `${this.divWidth / this.clientScale}px`;
        this.$refs.draw.style.height = `${this.divHeight / this.clientScale}px`;
      }
    },
    endDraw(e) {
      if (this.showDrawing) {
        e.preventDefault();
        this.dragging = false;
        this.showDrawing = false;
        this.$refs.draw.style.width = '0px';
        this.$refs.draw.style.height = '0px';
        this.$refs.draw.style.left = '0px';
        this.$refs.draw.style.top = '0px';
        this.$refs.main.style.cursor = 'default';
        this.chartList.forEach((v) => {
          if (v.type === this.selectTool) {
            this.addItem(this.selectTool, {
              x: this.divX,
              y: this.divY,
              w: this.divWidth / this.clientScale,
              h: this.divHeight / this.clientScale,
            });
          } else {
            v.selected = false;
          }
        });
        this.selectTool = null;
      }
    },
    onResize(size) {
      this.fullWidth = size.width;
      this.fullHeight = size.height;
    },
    minusScale() {
      this.scale -= 10;
      if (this.scale < 10) {
        this.scale = 10;
      }
    },
    plusScale() {
      this.scale += 10;
      if (this.scale > 300) {
        this.scale = 300;
      }
    },
    markDraw() {
      this.dragging = true;
      this.$refs.main.style.cursor = 'crosshair';
    },
    getScreen() {
      return this.$axios.get(`/bi/screen/queryById?id=${this.$route.query.id}`)
        .then(({ result }) => {
          const biScreen = result;
          this.name = biScreen.name;
          if (biScreen && biScreen.config) {
            const configObj = JSON.parse(biScreen.config);
            this.layout = configObj.layout;
            this.backgroundConfig = { zoom: 0.9, ...configObj.backgroundConfig };
            let maxIndex = 0;
            this.layout.forEach((item) => {
              if (item.type === 'group') {
                const chartArray = [];
                item.config.chartArray.forEach((chart) => {
                  chartArray.push(this.layout.find((im) => im.i === chart.i));
                });
                item.config.chartArray = chartArray;
              }
              if (item.i > maxIndex) {
                maxIndex = item.i;
              }
            });
            this.index = maxIndex + 1;
          }
          this.changeClientSize();
        }).catch(() => {
          this.$q.notify({
            color: 'red',
            message: '还原电子报告出错！',
          });
        });
    },
    selectChartIcon(type) {
      if (type === 'cursor') {
        this.selType = 'cursor';
      } else {
        this.markDraw();
        this.selectTool = type;
      }
      this.selChart = null;
      this.selChartArray = [];
    },

    selectItem(selItem) {
      if (selItem.config.fixedFreeze) {
        selItem.z = 0;
      }
      selItem.active = true;
      if (this.moving) {
        this.moving = false;
      }

      if (this.onCtrl && this.selChartArray.length > 0) {
        if (this.selChartArray.indexOf(selItem) < 0) {
          this.selChartArray.push(selItem);
          this.selChart = selItem;
        }
      } else {
        this.selChart = selItem;
        this.selChartArray = [selItem];
        if (selItem) {
          this.config = selItem.config;
          this.selType = selItem.type;
        }
      }
    },
    getItemClass(item) {
      const itemClsList = ['col', 'column'];
      if (item.type !== 'image') {
        itemClsList.push('draggable-item-class');
      } else if (item.config.overflowHiden) {
        itemClsList.push('draggable-item-class');
      }
      return itemClsList;
    },
    removeItem() {
      const selItem = this.selChart;
      if (selItem) {
        this.layout.splice(this.layout.indexOf(selItem), 1);
        this.selType = 'cursor';
      }
      this.addHistory();
    },
    addHistory() {
      if (this.history.length > 10) {
        this.history.shift();
      }
      this.history.push(JSON.stringify(this.layout));
      this.historyIndex = this.history.length - 1;
    },
    redo() {
      if (this.historyIndex < this.history.length - 1) {
        this.historyIndex += 1;
        this.layout = JSON.parse(this.history[this.historyIndex]);
      }
    },
    undo() {
      if (this.historyIndex > 0) {
        this.historyIndex -= 1;
        this.layout = JSON.parse(this.history[this.historyIndex]);
      }
    },
    applyIf(object, config) {
      if (object && config) {
        Object.keys(config).forEach((key) => {
          if (object[key] === undefined) {
            object[key] = config[key];
          } else if (typeof config[key] === 'object') {
            this.applyIf(object[key], config[key]);
          }
        });
      }
      return object;
    },
    autoFix() {
      if (this.layout.length > 0) {
        this.layout.forEach((item) => {
          this.applyIf(item.config, chartConfig(item.type, this.backgroundConfig.darkModel));
        });
        this.$info('修复完成');
      }
    },
    cutItem() {
      if (this.layout.filter((item) => item === this.selChart).length === 1) {
        [this.copyedItem] = (this.layout.filter((item) => item === this.selChart));
        this.removeItem();
      }
    },
    copyItem() {
      if (this.layout.filter((item) => item === this.selChart).length === 1) {
        [this.copyedItem] = (this.layout.filter((item) => item === this.selChart));
      }
    },
    pasteItem() {
      if (this.copyedItem) {
        this.index += 1;
        const copyObj = JSON.parse(JSON.stringify(this.copyedItem));
        copyObj.i = this.index;
        this.layout.push(copyObj);
        this.selectItem(this.index);
        this.addHistory();
      }
    },
    addItem(type, itemConfig) {
      const item = {
        x: itemConfig.x || 0,
        y: itemConfig.y || 0,
        w: itemConfig.w || 300,
        h: itemConfig.h || 100,
        type,
        name: type,
        z: 1000,
        active: true,
        i: this.index += 1,
      };
      if (type === 'group') {
        Object.assign(item, itemConfig);
      } else {
        item.config = chartConfig(type, this.backgroundConfig.darkModel);
      }
      this.layout.push(item);

      this.selectItem(item);
      this.addHistory();
    },
    viewScreen() {
      const { id } = this.$route.query;
      if (!id) {
        this.$q.notify({
          color: 'red',
          message: 'id not exist!',
        });
        return;
      }
      const { href } = this.$router.resolve({
        path: `/view?id=${id}`,
      });
      window.open(href, '_blank');
    },
    viewFullScreen() {
      const { id } = this.$route.query;
      const { href } = this.$router.resolve({
        path: `/viewfull?id=${id}`,
      });
      window.open(href, '_blank');
    },
    saveScreen() {
      const { id } = this.$route.query;
      if (!id) {
        this.$q.notify({
          color: 'red',
          message: 'id not exist!',
        });
        return;
      }
      this.saveLoading = true;
      const formData = {
        id,
        config: JSON.stringify({ layout: this.layout, backgroundConfig: this.backgroundConfig }),
      };
      this.$axios.put('/bi/screen/editConfig', formData).then((r) => {
        this.$q.notify(r.message);
      }).finally(() => {
        this.saveLoading = false;
      });
    },
    doSizeResize(cx, cy, cw, ch) {
      this.selChartArray.filter((idx) => idx.i === this.selChart.i).forEach((item) => {
        if (item) {
          this.sizeResizeItem(item, cx, cy, cw, ch);
        }
      });
      // 如果当前是组合对象，那么子对象也要一起动
      if (this.selChart.type === 'group') {
        this.selChart.config.chartArray.forEach((sub) => {
          this.sizeResizeItem(sub, cx, cy, cw, ch);
        });
      }
    },
    sizeResizeItem(item, cx, cy, cw, ch) {
      const clientWidth = this.backgroundConfig.width;
      const clientHight = this.backgroundConfig.height;
      // 调整尺寸
      item.x += cx;
      item.y += cy;
      item.w += cw;
      item.h += ch;
      // 边界检查，最小尺寸是12*12
      item.x = item.x < 0 ? 0 : item.x;
      item.x = item.x > clientWidth ? clientWidth - 12 : item.x;
      item.y = item.y < 0 ? 0 : item.y;
      item.y = item.y > clientHight ? clientHight - 12 : item.y;
      item.w = item.w < 12 ? 12 : item.w;
      item.h = item.h < 12 ? 12 : item.h;
    },

    resizedEvent(x, y, w, h) {
      const { selChart } = this;
      if (selChart) {
        this.sizeResize((x - selChart.x), (y - selChart.y), (w - selChart.w), (h - selChart.h));
        selChart.config.needResize = true;
      }
    },
    doOnDrag(x, y) {
      const { selChart } = this;
      this.moving = true;
      if (selChart) {
        this.sizeResize((x - selChart.x), (y - selChart.y), 0, 0);
        selChart.x = x;
        selChart.y = y;
      }
    },
    paramChange(param) {
      // 将请求参数合并到一起
      Object.assign(this.allParam, param);
      this.$root.$emit('allParamChange', this.allParam);
    },
    caclBackground() {

    },
    doMoveItem(dir, type) {
      this.selChartArray.forEach((item) => {
        const step = this.onShfit ? this.shiftMove : this.move;
        if (type === 'add') {
          item[dir] += step;
          this.sizeResize(dir === 'x' ? step : 0, dir === 'y' ? step : 0, dir === 'w' ? step : 0, dir === 'h' ? step : 0);
        } else if (item[dir] >= step) {
          item[dir] -= step;
          this.sizeResize(dir === 'x' ? -step : 0, dir === 'y' ? -step : 0, dir === 'w' ? -step : 0, dir === 'h' ? -step : 0);
        }
        if (dir === 'w' || dir === 'h') {
          item.config.needResize = true;
        }
      });
    },
    setKeyStatus(e, status) {
      if (this.selChartArray.filter((sel) => sel.active).length > 0) {
        if (e.code === 'ShiftLeft' || e.code === 'ShiftRight') {
          this.onShfit = status;
        }
        if (e.code === 'ControlLeft' || e.code === 'ControlRight') {
          this.onCtrl = status;
        }
        if (status) {
          switch (e.code) {
            case 'ArrowUp':
              if (this.onCtrl) {
                this.moveItem('h', 'sub');
              } else {
                this.moveItem('y', 'sub');
              }
              e.preventDefault();
              break;
            case 'ArrowDown':
              if (this.onCtrl) {
                this.moveItem('h', 'add');
              } else {
                this.moveItem('y', 'add');
              }
              e.preventDefault();
              break;
            case 'ArrowLeft':
              if (this.onCtrl) {
                this.moveItem('w', 'sub');
              } else {
                this.moveItem('x', 'sub');
              }
              e.preventDefault();
              break;
            case 'ArrowRight':
              if (this.onCtrl) {
                this.moveItem('w', 'add');
              } else {
                this.moveItem('x', 'add');
              }
              e.preventDefault();
              break;
            default:
              break;
          }
        }
      }
    },
    addFav() {
      this.$q.dialog({
        title: '添加到收藏夹',
        message: '请输入名称',
        prompt: {
          model: '',
          outlined: true,
          isValid: (val) => val.length > 2,
          type: 'text',
        },
        cancel: true,
        persistent: true,
      }).onOk((data) => {
        let type = 'complex';
        let config = JSON.stringify(this.selChartArray);
        if (this.selChartArray.length === 0) {
          type = 'cursor';
          config = JSON.stringify(this.backgroundConfig);
        } else if (this.selChartArray.length === 1) {
          type = this.selChartArray[0].type;
        }

        this.$axios.post('/bi/favorites/add', {
          name: data,
          type,
          config,
        }).then((r) => {
          this.$info(r.message);
          this.query();
        });
      });
    },
    fav() {
      this.$refs.favoriteDialog.show();
    },
    addFavorite(p) {
      if (p.type === 'cursor') {
        this.backgroundConfig = { ...JSON.parse(p.config) };
      } else {
        const favArray = JSON.parse(p.config);
        if (favArray.length > 0) {
          favArray.forEach((fav) => {
            if (fav.type === 'group') {
              fav.config.chartArray.forEach((chart) => {
                this.index += 1;
                chart.i = this.index;
                this.layout.push(chart);
              });
            }
            this.index += 1;
            fav.i = this.index;
            this.layout.push(fav);
            this.selectItem(fav);
            this.addHistory();
          });
        }
      }
    },
    selectClass(type) {
      if (this.dragging && type === this.selectTool) {
        return 'cc-bi-select-tool';
      }
      return '';
    },
  },
};
