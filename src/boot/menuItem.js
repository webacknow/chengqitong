export default
    [
      {
        redirect: "",
        path: "/home",
        component: "home",
        route: "1",
        meta: {
          keepAlive: false,
          internalOrExternal: false,
          icon: "mdi-home-outline",
          title: "首页"
        },
        name: "home",
        id: "home-101-id"
      },
      {
        redirect: "/member/list",
        path: "/member",
        component: "member",
        route: "1",
        children: [
          {
            path: "/member/list",
            component: "member/list",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "用户列表"
            },
            name: "member-list",
            id: "member-list-id"
          },
          {
            path: "/member/add",
            component: "member/add",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "用户列表"
            },
            name: "member-add",
            id: "member-add-id"
          },
          {
            path: "/member/detail",
            component: "member/detail",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "用户详情"
            },
            hidden: true,
            name: "member-detail",
            id: "member-detail"
          },
          {
            path: "/memberserver/detail",
            component: "memberserver/detail",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "服务商详情"
            },
            hidden: true,
            name: "memberserver-detail",
            id: "memberserver-detail"
          },
          {
            path: "/order/chargedetail",
            component: "order/chargedetail",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "订单详情"
            },
            hidden: true,
            name: "order-detail",
            id: "order-detail"
          },
          {
            path: "/order/chargelist",
            component: "order/chargelist",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "充值记录"
            },
            hidden: true,
            name: "order-chargelist",
            id: "order-chargelist"
          },
        ],
        meta: {
          keepAlive: false,
          internalOrExternal: false,
          icon: "mdi-account",
          title: "员工管理"
        },
        name: "member",
        id: "member-101-id"
      },
      {
        redirect: "/shop/list",
        path: "/stocklog",
        component: "stocklog",
        route: "1",
        children: [
          {
            path: "/shop/list",
            component: "shop/list",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "公司列表"
            },
            name: "shop-list",
            id: "shop-list-id"
          },
          {
            path: "/shop/add",
            component: "shop/add",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "添加公司"
            },
            name: "shop-add",
            id: "shop-add-id"
          },
          {
            path: "/shop/edit",
            component: "shop/edit",
            props: false,
            route: "1",
            hidden: true,
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "公司编辑"
            },
            name: "shop-edit",
            id: "shop-edit-id"
          },
          {
            path: "/shop/detail",
            component: "shop/detail",
            props: false,
            route: "1",
            hidden: true,
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "公司详情"
            },
            name: "shop-detail",
            id: "shop-detail-id"
          }

        ],
        meta: {
          keepAlive: false,
          internalOrExternal: false,
          icon: "mdi-abjad-hebrew",
          title: "公司管理"
        },
        name: "shop",
        id: "shop-main-id"
      },
      // {
      //   redirect: "/goods/list",
      //   path: "/goods",
      //   component: "goodslist",
      //   route: "1",
      //   children: [
      //     {
      //       path: "/list",
      //       component: "goods/list",
      //       props: false,
      //       route: "1",
      //       meta: {
      //         keepAlive: false,
      //         internalOrExternal: false,
      //         title: "短信列表"
      //       },
      //       name: "stocklog-list",
      //       id: "stocklog-list-id"
      //     }
      //
      //   ],
      //   meta: {
      //     keepAlive: false,
      //     internalOrExternal: false,
      //     icon: "mdi-abjad-hebrew",
      //     title: "短信管理"
      //   },
      //   name: "stocklog",
      //   id: "stocklog-main-id-2"
      // },
      {
        redirect: "",
        path: "/department",
        component: "department",
        route: "1",
        children: [
         {
            path: "/department/list",
            component: "department/list",
            props: false,
            route: "118",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "组织信息"
            },
            name: "department-list",
            id: "department-list-id"
          }
        ],
        meta: {
          keepAlive: false,
          internalOrExternal: false,
          icon: "mdi-abugida-thai",
          title: "组织管理"
        },
        name: "orderManage",
        id: "order-main-id"
      },
      {
        redirect: "/ticket/list",
        path: "/ticket",
        component: "ticket",
        route: "1",
        children: [
          {
            path: "/order/list",
            component: "order/list",
            props: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "短信发送记录列表"
            },
            name: "ticket-list",
            id: "ticket-list-id"
          },
          {
            path: "/goods/list",
            component: "goods/list",
            props: false,
            hidden: false,
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "短信模板管理"
            },
            name: "ticket-ticketautomake",
            id: "ticket-ticketautomake-id"
          }
          // {
          //   path: "/ticketlog/list",
          //   component: "ticketlog/list",
          //   props: false,
          //   route: "1",
          //   meta: {
          //     keepAlive: false,
          //     internalOrExternal: false,
          //     title: "红包领取明细"
          //   },
          //   name: "ticket-list-all",
          //   id: "ticket-list-all"
          // }

        ],
        meta: {
          keepAlive: false,
          internalOrExternal: false,
          icon: "mdi-access-point",
          title: "短信管理"
        },
        name: "stock",
        id: "stockID"
      },
      {
        redirect: "/role/list",
        path: "/mnt",
        component: "mnt",
        route: "1",
        children: [
          // {
          //   path: "/role/list",
          //   component: "role/list",
          //   route: "1",
          //   meta: {
          //     keepAlive: false,
          //     internalOrExternal: false,
          //     title: "角色管理"
          //   },
          //   name: "role-list",
          //   id: "134397773"
          // },
          {
            path: "/admin/list",
            component: "admin/list",
            route: "1",
            meta: {
              keepAlive: false,
              internalOrExternal: false,
              title: "管理员管理"
            },
            name: "admin-list",
            id: "139021839876333977733"
          },

        ],
        meta: {
          keepAlive: false,
          internalOrExternal: false,
          icon: "mdi-access-point-network",
          title: "账号中心"
        },
        name: "mnt",
        id: "139021785589037875412"
      }


    ];




