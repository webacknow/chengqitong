module.exports = function a(ctx) {
  return {
    supportTS: false,
    supportIE: false,
    boot: [
      'index',
      'filter',
      'msg',
      '/api',
    ],
    css: [
      'app.styl',
    ],
    extras: [
      'roboto-font',
      'material-icons',
      'mdi-v5',
    ],
    framework: {
      iconSet: 'material-icons',
      lang: 'zh-hans',
      importStrategy: 'auto',
      plugins: [
        'Notify',
        'Dialog',
        'AppFullscreen',
        'Loading',
      ],
      config: {
        notify: {
          position: 'top',
          timeout: 4000,
          color: 'info',
          icon: 'info',
          actions: [{ icon: 'close', color: 'white' }],
        },
      },
      components: ['QRadio','QField','QSelect','QBtnGroup','QBtn','QToggle' ,'QImg','QUploader' ,'QImg' , 'QAvatar', 'QEditor']
    },
    animations: [],
    build: {
      // vueRouterMode: 'history',
      transpile: true, // 执行babel转码
      vueCompiler: false, // 仅 Vue运行时
      scopeHoisting: true, // 提升运行时性能
      analyze: false, // 显示分析
      modern: true, // ES6
      extractCSS: false, // 从Vue文件中提取CSS
      minify: true, // 缩小代码
      preloadChunks: true, // 预加载
      sourceMap: ctx.dev,
      gzip: false,
      extendWebpack(cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/,
        });
      },
      env: {
        PRODUCT_NAME: '管理平台',
        BASE_URL: 'http://file.gzzsu.com/shop109',
        WEB_SOCKET: 'ws://cc-admin.top:8899',
      },
    },
    devServer: {
      https: false,
      port: 9667,
      open: true,
      proxy: {
        '/ashop': {
          // http://admin.dev.ssgfgtfy.com/ashop105/ticketlist
          // target: 'http://admin.dev.ssgfgtfy.com',
          target: 'http://file.gzzsu.com/shop109',
          // target: 'http://localhost:8106/shop105',
          // target: 'https://api.ssgfgtfy.com',
          // target: 'https://api.dev.ssgfgtfy.com',
          // target: 'http://localhost:8106/ashop105',
          // target: 'https://api.dev.ssgfgtfy.com',
          changeOrigin: true,
          // pathRewrite:{
          //   '^/ashop108': ''
          // }
        },
      }
    },
  };
};
